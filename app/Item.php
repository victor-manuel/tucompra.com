<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Image;

class Item extends Model
{
    protected $fillable = ['nombre', 'description', 'precing', 'image_id'];  
    public $timestamps = false; 

    public function image(){
         return $this->hasOne(Image::class);
    }
}
