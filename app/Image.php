<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Item;


class Image extends Model
{
    protected $fillable = [ 'image'];

    public function item(){
        return $this->hasOne(Item::class);
    }

}
