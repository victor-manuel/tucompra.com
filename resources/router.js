import Vue from "vue";
import Router from "vue-router";
import Vuetify from "vuetify";

Vue.use(Router);
Vue.use(Vuetify);

export default new Router({
    routes: [
        {
            path: "/",
            name: "index",
            component: require("./views/views/index.vue").default
        },
        {
            path: "/items",
            name: "items",
            component: require("./views/views/item.vue").default
        },
     
    ],
    mode: "history"
});
