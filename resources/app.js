

require('./bootstrap');

window.Vue = require('vue');


Vue.component('dashboard-component', require('./views/dashboard/Index.vue').default);

import router from './router.js';
import store from './store.js';
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min';
import Axios from 'axios';
import i18n from './i18n';
import 'vuetify/dist/vuetify.min.css';
import 'material-design-icons-iconfont/dist/material-design-icons.css';


Vue.config.productionTip = false
Vue.use(Vuetify)
export default new Vuetify({ })

const app = new Vue({
    el: '#app',
    vuetify: new Vuetify(),
    Axios,
    store,
    router,
    i18n,
});
